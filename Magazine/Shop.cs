﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Urok17;
using System.Runtime.CompilerServices;
using System.ComponentModel;

namespace Magazine
{
    public class Shop : INotifyPropertyChanged
    {
        /// <summary>
        /// Внутреннее хранилище продуктов
        /// </summary>
        public Storage Storage
        {
            get { return _storage; }
            set
            {
                if (value != _storage)
                {
                    _storage = value;
                    NotifyPropertyChanged();
                }
            }
        }
        private Storage _storage;

        /// <summary>
        /// Количество продуктов в хранилище. Рекомендуется только читать!
        /// </summary>
        public int StorageCount
        {
            get { return _storageCount; }
            set
            {
                if (value != _storageCount)
                {
                    _storageCount = value;
                    NotifyPropertyChanged();
                }
            }
        }
        private int _storageCount;

        /// <summary>
        /// Название магазина
        /// </summary>
        public string Name
        {
            get { return _name; }
            set { if (value != _name) { _name = value; NotifyPropertyChanged(); } }
        }
        private string _name;

        /// <summary>
        /// Бюджет магазина
        /// </summary>
        public double Money
        {
            get { return _money; }
            set { if (value != _money) { _money = value; NotifyPropertyChanged(); } }
        }
        private double _money;

        /// <summary>
        /// Возвращает новый список типов продуктов, хранимых в Storage
        /// </summary>
        public List<string> ProductTypes
        {
            get { return new List<string>(_productTypes); }
        }
        private List<string> _productTypes;

        /// <summary>
        /// Возвращает новый список расширенных типов, хранимых в Storage
        /// </summary>
        public Dictionary<string, List<string>> ProductsSubTypes 
        {
            get { return new Dictionary<string, List<string>>(_productsSubTypes); } 
        }
        private Dictionary<string, List<string>> _productsSubTypes;



        public Shop()
        {
            Initialization();
        }
        public Shop(string name, int capital)
        {
            this.Name = name;
            this.Money = capital;
            Initialization();
        }

        private void Initialization()
        {
            _productTypes = new List<string>();
            _productsSubTypes = new Dictionary<string, List<string>>();
            _storage = new Storage();
            _storage.CollectionChanged += _storage_CollectionChanged;
        }



        /// <summary>
        /// Обработчик пробрасывает оповещения об изменениях в Storage наружу
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void _storage_CollectionChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
        {
            StorageCount = _storage.Count;
            NotifyPropertyChanged("Storage");
        }



        /// <summary>
        /// Добавляет типы продукции, используемые магазином (и к тем, и к тем)
        /// </summary>
        /// <param name="types">Список основных типов</param>
        /// <param name="subtypes"> Список расширенных типов</param>
        private void SetupSubTypes(List<string> types, List<List<string>> subtypes)
        {
            for (int i = 0; i < types.Count; i++)
                _productTypes.Add(types[i]);

            for (int i = 0; i < _productTypes.Count; i++)
            {
                _productsSubTypes.Add(_productTypes[i], subtypes[i]);
            }
            NotifyPropertyChanged("ProductTypes");
            NotifyPropertyChanged("ProductsSubTypes");
        }
        /// <summary>
        /// Добавляет типы продукции, используемые магазином
        /// </summary>
        /// <param name="allProductTypes">Подготовленный список продуктов</param>
        private void SetupSubTypes(IEnumerable<ProductType> allProductTypes)
        {
            if (allProductTypes == null) throw new ArgumentException("allProductTypes");

            List<string> ls = allProductTypes.Select((t) => { return t.Name; }).Distinct().ToList();

            //for (int i = 0; i < ls.Count; i++)
            //    _productTypes.Add(ls[i]);

            List<List<string>> lss = new List<List<string>>();
            foreach (string s in ls)
                lss.Add(allProductTypes.Where(t => t.Name == s).Select(x => x.SubType).ToList());

            //= allProductTypes.Select(
            //   t=>{
            //       return allProductTypes.Where(sub=>sub.Name == t.Name).Select(s=>s.SubType).ToList();
            //   }).Distinct().ToList();



            ////this._productsSubTypes = new Dictionary<string, List<string>>();

            //for (int i = 0; i < _productTypes.Count; i++)
            //{
            //    _productsSubTypes.Add(_productTypes[i], lss[i]);
            //}
            //   // allProductTypes.ToDictionary<List<string>, string>(t => { return t.Name; });

            SetupSubTypes(ls, lss);
        }

        /// <summary>
        /// Заново устанвливает основные и расширенные типы продуктов в магазин.
        /// </summary>
        /// <param name="types">Список основных типов</param>
        /// <param name="subtypes"> Список расширенных типов</param>
        public void SetupTypes(List<string> types, List<List<string>> subtypes)
        {
            _productsSubTypes.Clear();
            _productTypes.Clear();

            SetupSubTypes(types, subtypes);
        }
        /// <summary>
        /// Заново устанвливает основные и расширенные типы продуктов в магазин.
        /// </summary>
        /// <param name="allProductTypes">Подготовленный список типов</param>
        public void SetupTypes(IEnumerable<ProductType> allProductTypes)
        {
            _productsSubTypes.Clear();
            _productTypes.Clear();
            SetupSubTypes(allProductTypes);
        }



        /// <summary>
        /// Добавить продукт в список основных типов ProductsTypes
        /// </summary>
        /// <param name="type">Название добавляемого типа</param>
        public virtual void AddProd(string type)
        {
            if (true == ProductTypes.Contains(type))
                throw new ArgumentException(string.Format("\'{0}\' уже содержится к списке типов.", type));


            _productTypes.Add(type);
            _productsSubTypes.Add(type, new List<string>());

            NotifyPropertyChanged("ProductTypes");
            NotifyPropertyChanged("ProductsSubTypes");

        }
        /// <summary>
        /// Удалить продукт из списка основных типов ProductsTypes
        /// </summary>
        /// <param name="type">Название удаляемого типа</param>
        public virtual void RemoveProd(string type)
        {
            if (ProductTypes.Contains(type) == false)
                throw new ArgumentOutOfRangeException("type", string.Format("\'{0}\' не содержится к списке типов.", type));


            _productTypes.Remove(type);
            _productsSubTypes.Remove(type);

            NotifyPropertyChanged("ProductTypes");
            NotifyPropertyChanged("ProductsSubTypes");

        }
        /// <summary>
        /// Добавить продукт в списка дополнительных типов ProductsSubTypes
        /// </summary>
        /// <param name="subtype">Название добавляемого типа</param>
        public virtual void AddSubType(string type, string subtype)
        {
            if (false == ProductsSubTypes.Keys.Contains(type))
                throw new ArgumentOutOfRangeException("type", string.Format("Тип \'{0}\' не содержится к списке типов.", type));

            if (true == ProductsSubTypes[type].Contains(subtype))
                throw new ArgumentException(string.Format("\'{0}\' уже содержится к списке подтипов.", subtype));


            ProductsSubTypes[type].Add(subtype);
            NotifyPropertyChanged("ProductsSubTypes");
        }
        /// <summary>
        /// Удалить продукт из списка дополнительных типов ProductsSubTypes
        /// </summary>
        /// <param name="subtype">Название удаляемого типа</param>
        public virtual void RemoveSubType(string type, string subtype)
        {
            if (false == ProductsSubTypes.Keys.Contains(type))
                throw new ArgumentOutOfRangeException("type", string.Format("Тип \'{0}\' не содержится к списке типов.", type));

            if (false == ProductsSubTypes[type].Contains(subtype))
                throw new ArgumentOutOfRangeException("subtype", string.Format("\'{0}\' не содержится к списке подтипов.", subtype));


            ProductsSubTypes[type].Remove(subtype);
            NotifyPropertyChanged("ProductsSubTypes");
        }


        /// <summary>
        /// Событие происходит при изменение каждого свойства
        /// </summary>
        public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;
        // атрибут CallerMemberName позволяет не писать каждый раз имя проперти вручную
        private void NotifyPropertyChanged([CallerMemberName] String propertyName = "")
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

    }

}
