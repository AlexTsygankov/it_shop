﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Data.SqlClient;
using System.Data;

namespace Magazine
{
    // Мог бы быть структурой, но нужны конструкторы
    /// <summary>
    /// Общий класс для типов продуктов
    /// </summary>
    public class ProductType
    {
        private string _name;
        private string _subType;

        /// <summary>
        /// Остновной тип продукта
        /// </summary>
        public string Name
        {
            get
            {
                return _name;
            }
            set {
                _name = value;
            }
        }
        /// <summary>
        /// Расширенный тип продукта
        /// </summary>
        public string SubType
        {
            get
            {
                return _subType;
            }
            set
            {
                _subType = value;
            }
        }

        /// <summary>
        /// Создаёт тип продукта по заданному основному типу и расширенному.
        /// </summary>
        /// <param name="name">Название основного типа</param>
        /// <param name="subname">Расширенный тип. (Опционально)</param>
        public ProductType(string name, string subname)
        {
            this._name = name;
            this._subType = subname;
        }
        public ProductType()
        {
            _name = "";
            _subType = "";
        }

        public override string ToString()
        {
            return string.Format("{0}.{1}", this._name.Trim(), _subType.Trim());
        }
    }
}
