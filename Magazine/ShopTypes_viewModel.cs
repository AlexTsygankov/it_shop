﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.ComponentModel;
using System.Runtime.CompilerServices;

using System.IO;
using System.Xml.Serialization;

namespace Magazine
{
    /// <summary>
    /// Представление модели под конкретную формочку. Имеет удобные методы и свойства для работы с моделью.
    /// Поcледный раз использовался в редакторе типов
    /// </summary>
    public class ShopTypes_viewModel : INotifyPropertyChanged, IModelSetter<Shop>
    {
        /// <summary>
        /// Список основных типов продуктов
        /// </summary>
        public List<string> ProductTypes
        {
            //get { return _productTypes; }
            //set
            //{
            //    if (value != _productTypes)
            //    {
            //        _productTypes = value;
            //        NotifyPropertyChanged();
            //    }
            //}
            get { return _model.ProductTypes; }
        }
        //private List<string> _productTypes;

        /// <summary>
        /// Расширенный список продукции. Хранит список подтипов последнего выбранного типа продукции
        /// </summary>
        public List<string> ProductsSubTypes 
        {
            get { return _productsSubTypes; }
            private set
            {
                if (value != _productsSubTypes)
                {
                    _productsSubTypes = value;
                    NotifyPropertyChanged();
                }
            }
        }
        private List<string> _productsSubTypes;

        /// <summary>
        /// Основное локальное хранилище типов из модели.
        /// </summary>
        protected Dictionary<string, List<string>> _productsSubTypes_storage;
        protected Shop _model;



        protected int lastIndex { get; set; }
        /// <summary>
        /// Производит смену последнего вбранного типа продукта. В соответсвии с этим, меняет 
        /// свойство ProductsSubTypes
        /// </summary>
        /// <param name="index">Индекс продукта. Допустимое значение  [-1; ProductsTypes.Count]</param>
        public void ChangeProductTypeSelectedIndex(int index)
        {
            if (index < -1 || index >= ProductTypes.Count)
                throw new ArgumentException("index");

            lastIndex = index == -1 ?
                0 : index;

            if (lastIndex > ProductTypes.Count)
                ProductsSubTypes = null;
            else
                ProductsSubTypes = _model.ProductsSubTypes[ProductTypes[lastIndex]];
        }



        /// <summary>
        /// Событие происходит при изменение каждого свойства
        /// </summary>
        public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;
        // атрибут CallerMemberName позволяет не писать каждый раз имя проперти вручную
        protected void NotifyPropertyChanged([CallerMemberName] String propertyName = "")
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }
        
        private void _model_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            this.NotifyPropertyChanged(e.PropertyName);
        }
       
        /// <summary>
        /// Корректно устанавливает модель для её последующего отображения в представлении
        /// </summary>
        /// <param name="model">Ваш магазин</param>
        public void SetModel(Shop model)
        {
            if (model == null) throw new ArgumentNullException("model");

            _model = model;
            _model.PropertyChanged += _model_PropertyChanged;

            InitTypeProperties();
        }
        private void InitTypeProperties()
        {
            //ProductTypes = _model.ProductTypes;
            NotifyPropertyChanged("ProductTypes");
            _productsSubTypes_storage = _model.ProductsSubTypes;

            if (_productsSubTypes_storage.Count > 0)
                ChangeProductTypeSelectedIndex(0);
        }

        /// <summary>
        /// Добавить продукт в список основных типов ProductsTypes
        /// </summary>
        /// <param name="type">Название добавляемого типа</param>
        public virtual void AddProd(string type)
        {
            if (true == ProductTypes.Contains(type))
                throw new ArgumentException(string.Format("\'{0}\' уже содержится к списке типов.", type));

            try { 
                DBManager.getInstance().AddType(type);
                _model.AddProd(type);
            }
            catch (Exception ex) { throw; }

            //ProductTypes.Add(type);
            //_productsSubTypes_storage.Add(type, new List<string>());

            NotifyPropertyChanged("ProductTypes");
            ChangeProductTypeSelectedIndex(lastIndex);
            NotifyPropertyChanged("ProductsSubTypes");

        }
        /// <summary>
        /// Удалить продукт из списка основных типов ProductsTypes
        /// </summary>
        /// <param name="type">Название удаляемого типа</param>
        public virtual void RemoveProd(string type)
        {
            if (false == ProductTypes.Contains(type))
                throw new ArgumentException(string.Format("Тип \'{0}\' отсутствует в списке типов.", type));

            try { 
                DBManager.getInstance().RemoveType(type);
                _model.RemoveProd(type);
            }
            catch (Exception ex) { throw; }

            //ProductTypes.Remove(type);
            //_productsSubTypes_storage.Remove(type);

            NotifyPropertyChanged("ProductTypes");
            ChangeProductTypeSelectedIndex(0);
            NotifyPropertyChanged("ProductsSubTypes");

        }
        /// <summary>
        /// Добавить продукт в списка дополнительных типов ProductsSubTypes
        /// </summary>
        /// <param name="subtype">Название добавляемого типа</param>
        public virtual void AddSubType(string subtype)
        {

            try { 
                DBManager.getInstance().AddSubtype(subtype, ProductTypes[lastIndex]);
                _model.AddSubType(ProductTypes[lastIndex], subtype);
            }
            catch (Exception ex) { throw; }

            ChangeProductTypeSelectedIndex(lastIndex);
            NotifyPropertyChanged("ProductsSubTypes");
        }
        /// <summary>
        /// Удалить продукт из списка дополнительных типов ProductsSubTypes
        /// </summary>
        /// <param name="subtype">Название удаляемого типа</param>
        public virtual void RemoveSubType(string subtype)
        {

            try
            {
                DBManager.getInstance().RemoveSubtype(subtype, ProductTypes[lastIndex]);
                _model.RemoveSubType(ProductTypes[lastIndex], subtype);
            }
            catch (Exception ex) { throw; }

            ChangeProductTypeSelectedIndex(lastIndex);
            NotifyPropertyChanged("ProductsSubTypes");
        }



        /// <summary>
        /// Загрузка типов в модель из XML файла
        /// </summary>
        /// <param name="path">Путь к XML сериализованному файлу</param>
        public void LoadTypesFromFile(string path)
        {
            using (StreamReader reader = new StreamReader(path))
            {
                XmlSerializer serializer = new XmlSerializer(typeof(List<ProductType>));
                List<ProductType> types = (List<ProductType>)serializer.Deserialize(reader);

                _model.SetupTypes(types);
                InitTypeProperties();
            }
            NotifyPropertyChanged("ProductTypes");
            NotifyPropertyChanged("ProductsSubTypes");
        }

        /// <summary>
        /// Сериализация типов модели в XML файл
        /// </summary>
        /// <param name="path"> Путь к файлу, включающий название файла</param>
        public void SaveTypesToFile(string path)
        {
            List<ProductType> ls = new List<ProductType>();
            foreach (string pr in ProductTypes)
            {
                if (_productsSubTypes_storage.ContainsKey(pr))
                {
                    foreach (string tp in _productsSubTypes_storage[pr])
                        ls.Add(new ProductType(pr, tp));
                }
                else
                    ls.Add(new ProductType(pr, ""));
            }

            using (StreamWriter writer = new StreamWriter(path))
            {
                XmlSerializer serializer = new XmlSerializer(typeof(List<ProductType>));
                serializer.Serialize(writer, ls);
            }
        }
    }

}
