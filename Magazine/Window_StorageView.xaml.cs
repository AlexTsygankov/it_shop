﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

using System.IO;
using System.Xml.Serialization;
using System.Data.SqlClient;
using System.Data;

using Urok17;

namespace Magazine
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class Window_Storage : Window, IModelSetter<Shop>
    {
        public ShopContainer ShopContainer { get { return (ShopContainer)Resources["MagazinResource"]; } }
        private Shop Model { get { return ShopContainer.Model; } }


        private Products AddingProductsBuffer;

        public Window_Storage()
        {
            InitializeComponent();
            AddingProductsBuffer = new Products();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            if (txbxPrice.Text != "" && txbxWeight.Text != "" && cmbbxType.SelectedItem != null)
            {
                double num;
                if (!double.TryParse(txbxWeight.Text.Replace('.', ','), out num))
                {
                    MessageBox.Show("Вес должен быть вещественным числом", "Неверно указан вес");
                    return;
                }

                AddingProductsBuffer.weightProperti = num;

                if (!double.TryParse(txbxPrice.Text.Replace('.', ','), out num))
                {
                    MessageBox.Show("Цена должна быть вещественным числом", "Неверно указана цена");
                    return;
                }
                AddingProductsBuffer.priceProperti = num;
                AddingProductsBuffer.Producer = txbxProducer.Text;

                
                Model.Storage.Add(AddingProductsBuffer);
                AddingProductsBuffer = new Products();

            }

            
        }
        private void ButtonLoadTypes_Click(object sender, RoutedEventArgs e)
        {            
            Window_TypesEditor view = new Window_TypesEditor();
            view.SetModel(this.ShopContainer.Model);
            view.ShowDialog();
        }

        private void cmbbxProd_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            string s = (string)((ComboBox)sender).SelectedItem;
            
            try
            {
                this.cmbbxType.ItemsSource = Model.ProductsSubTypes[s];
                this.cmbbxType.SelectedIndex = 0;                
                AddingProductsBuffer.Type.Name = s;
                //ViewModel.ChangeProductTypeSelectedIndex(cmbbxType.SelectedIndex);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void cmbbxType_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            AddingProductsBuffer.Type.SubType = (string)((ComboBox)sender).SelectedItem;
        }

       
        /// <summary>
        /// Устанавливает модель магазина в контейнер зависимостей
        /// </summary>
        /// <param name="model"></param>
        public void SetModel(Shop model)
        {
            try
            {
                model.SetupTypes(DBManager.getInstance().GetProductTypes());
                ShopContainer.Model = model;

                cmbbxProd.ItemsSource = Model.ProductTypes;
                ShopContainer.Model.PropertyChanged += Model_PropertyChanged;
            }
            catch (Exception ex)
            {
                MessageBox.Show(
                    string.Format("Возникла ошибка при подключении к базе данных\r\n\"{0}\"", ex.Message)
                    , "Ошибка"
                    , MessageBoxButton.OK,
                    MessageBoxImage.Error);
                //this.Close(); 
            }
        }

        void Model_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            dtgrdStorage.Items.Refresh();
            cmbbxProd.ItemsSource = Model.ProductTypes;
            cmbbxProd.Items.Refresh();
            //cmbbxType.Items.Refresh();
        }
    }

        
}
