﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Windows.Data;
using Urok17;

namespace Magazine
{
    public class MyStringConverter : IValueConverter
    {
        //private string rusAlphabet = "абвгдеёжзийклмпнопрстуфхцчшщъыьэюя";//АБВГДЕЁЖЗИЙКЛМНОПРСТУФХЦЧШЩЪЫЬЭЮЯ";

        /// <summary>
        /// Таблица транслита русских букв
        /// </summary>
        private Dictionary<char, string> translit = new Dictionary<char, string> {
            {'а',"a"},  {'б',"b"},  {'в',"v"},  {'г',"g"},  {'д',"d"},  {'е',"e"},
            {'ё',"jo"}, {'ж',"zh"}, {'з',"z"},  {'и',"i"},  {'й',"j"},  {'к',"k"},  
            {'л',"l"},  {'м',"m"},  {'н',"n"},  {'о',"o"},  {'п',"p"},  {'р',"r"},
            {'с',"s"},  {'т',"t"},  {'у',"u"},  {'ф',"f"},  {'х',"h"},  {'ц',"c"},
            {'ч',"ch"}, {'ш',"sh"}, {'щ',"shh"},    {'ъ',"#"},  {'ы',"y"},  {'ь',"'"},
            {'э',"je"}, {'ю',"ju"}, {'я',"ja"}
        };

        public virtual object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (value != null)
            {
                if (value is string)
                {
                    string str = (string)value;
                    //str = str.Length > 10 ?               // возможно не дано обрезать ВСЕ строки. ТАкое поведение оставил конвертору производителя
                    //            str.Substring(0, 10) :
                    //            str;

                    for(int i = 0, increment; i < str.Length; i += increment)
                    {
                        increment = 1;
                        if (translit.Keys.Contains(str[i]))
                        {
                            increment = translit[str[i]].Length;
                            str = str.Insert(i,translit[str[i]]);
                            str = str.Remove(i+increment, 1);
                        }
                        
                    }
                    return str;
                }
            }
            return value;
        }

        public virtual object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    public class MyPriceConverter : IValueConverter
    {

        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (value != null)
            {
                if (value is double)
                {
                    //double num;
                   // if (double.TryParse((string)value, out num))
                    //{
                        if ((double)value > 10000)
                            return "#FF0000";
                   // }
                }
                else if (value is Products)
                {
                    if (((Products)value).priceProperti > 10000)
                        return "#FF0000";
                }
            }
            return value;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
    public class MyProducerConverter : MyStringConverter
    {

        public override object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (value != null)
            {
                if (value is string)
                {
                    string translatedString = (string)base.Convert(value, targetType, parameter, culture);
                    if (translatedString.Length > 10)
                    {
                        translatedString = translatedString.Remove(10, translatedString.Length - 10) + "...";
                    }
                    return translatedString;
                }
            }
            return value;
        }

        public override object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
    public class MyWeightConverter : IValueConverter
    {

        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (value != null)
            {
                if (value is Products)
                {
                    if (((Products)value).weightProperti > 10)
                        return "#0000AA";
                }
            }
            return value;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    
}
