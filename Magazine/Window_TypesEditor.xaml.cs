﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

using System.IO;
using System.Xml.Serialization;
using System.ComponentModel;
using System.Collections.Specialized;

using Urok17;


namespace Magazine
{
    ///<summary>
    /// Логика взаимодействия для TypesEditor.xaml
    ///</summary>
    public partial class Window_TypesEditor : Window, IModelSetter<Shop>
    {
        public ShopTypes_viewModel ViewModel
        {
            get { return (ShopTypes_viewModel)Resources["MagazinModelResource"]; }
        }


        public Window_TypesEditor()
        {
            InitializeComponent();
        }

        private void btnAddProd_Click(object sender, RoutedEventArgs e)
        {
            AddType();
        }

        /// <summary>
        /// Type
        /// </summary>
        private void AddType()
        {
            if (txbxProd.Text.Length > 0
                && false == ViewModel.ProductTypes.Contains(txbxProd.Text))
            {
                try
                {                    
                    ViewModel.AddProd(txbxProd.Text);

                    txbxProd.Text = "";
                    lsbxProd.Items.Refresh();
                }
                catch (Exception ex) { MessageBox.Show(ex.Message); }
            }
        }

        private void AddSubtype()
        {
            if (lsbxProd.SelectedItem != null
                && txbxType.Text.Length > 0
                && ViewModel.ProductsSubTypes.Contains(txbxType.Text) == false)
            {
                try
                {
                    ViewModel.AddSubType(txbxType.Text);


                    txbxType.Text = "";
                    lsbxType.Items.Refresh();
                }
                catch (Exception ex) { MessageBox.Show(ex.Message); }
            }
        }

        private void btnAddType_Click(object sender, RoutedEventArgs e)
        {
            AddSubtype();
        }



        private void lsbxProd_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Delete && lsbxProd.SelectedIndex >= 0)
            {
                try
                {
                    ViewModel.RemoveProd((string)lsbxProd.SelectedItem);
                    lsbxProd.Items.Refresh();
                }
                catch (Exception ex) { MessageBox.Show(ex.Message); }
            }
        }

        private void lsbxType_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Delete && lsbxType.SelectedIndex >= 0)
            {
                try
                {
                    ViewModel.RemoveSubType((string)lsbxType.SelectedItem);

                    lsbxType.Items.Refresh();
                }
                catch (Exception ex) { MessageBox.Show(ex.Message); }
            }
        }
        private void txbxProd_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
                AddType();
        }

        private void txbxType_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
                AddSubtype();
        }

        /// <summary>
        /// Устанавливает модель для отображения
        /// </summary>
        /// <param name="magaz">Модель магазина</param>
        public void SetModel(Shop magaz)
        {
            try
            {
                magaz.SetupTypes(DBManager.getInstance().GetProductTypes());
                ViewModel.SetModel(magaz);
            }
            catch (Exception ex) { 
                MessageBox.Show(
                    string.Format("Возникла ошибка при подключении к базе данных\r\n\"{0}\"",ex.Message)
                    , "Ошибка"
                    ,MessageBoxButton.OK,
                    MessageBoxImage.Error); 
                //this.Close(); 
            }
        }

        private void MenuItem_Click(object sender, RoutedEventArgs e)
        {
            string path = Environment.CurrentDirectory + @"\types.txt";
            try
            {
                ViewModel.LoadTypesFromFile(path);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void lsbxProd_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            //try
            //{
            ViewModel.ChangeProductTypeSelectedIndex(lsbxProd.SelectedIndex);

            //}
            //catch (KeyNotFoundException ex)
            //{
            //    //MessageBox.Show("Этот продукт ещё не содержит подвидов. Обязательно укажите хотя бы 1 вид.");
            //    ProductsSubTypes = null;
            //}
        }

        private void MenuItem_Click_1(object sender, RoutedEventArgs e)
        {
            string path = Environment.CurrentDirectory + @"\types.txt";
            ViewModel.SaveTypesToFile(path);
        }


        private Window_UpdateType contextWindow;
        private void TextBlock_MouseRightButtonDown(object sender, MouseButtonEventArgs e)
        {
            if(sender is TextBlock)
            {
                if(contextWindow == null) contextWindow = new Window_UpdateType();

                contextWindow.Sender = (TextBlock)sender;
                contextWindow.Owner = this;

                contextWindow.Closed += win_Closed;
                contextWindow.ShowDialog();
             }
        }

        public void ChangeTextBlockText(TextBlock sender, string oldValue, string newValue)
        {
            try
            {
                if (UpdateOldvalueInDB(oldValue, newValue) <= 0)
                    MessageBox.Show("В базе строка не поменялась!",
                        "Предупреждение",
                        MessageBoxButton.OK,
                        MessageBoxImage.Warning);
            }
            catch (Exception ex) { MessageBox.Show(ex.Message, "Ошибка"); }
        }

        private int UpdateOldvalueInDB(string oldValue, string newValue)
        {
            if (ViewModel.ProductsSubTypes.Contains(oldValue))
                return DBManager.getInstance().UpdateSubtypes(oldValue, newValue);
            else if (ViewModel.ProductTypes.Contains(oldValue))
                return DBManager.getInstance().UpdateTypes(oldValue, newValue);
            else throw new ArgumentException("Значение отсутствует в списках");
        }

        void win_Closed(object sender, EventArgs e)
        {
            contextWindow = null;
        }

        private void Window_Closed(object sender, EventArgs e)
        {
            if (contextWindow != null)
                contextWindow.Close();
        }
    }
}
