﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Magazine
{
    /// <summary>
    /// Предоставляет интерфейс для установки модели
    /// </summary>
    /// <remarks></remarks>
    public interface IModelSetter<T>
    {        
        /// <summary>
        /// Содержит логику для установки model
        /// </summary>
        /// <param name="model"></param>
        void SetModel(T model);
    }
}
