﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

using Urok17;

namespace Magazine
{
    /// <summary>
    /// Логика взаимодействия для ProcurementScheduler.xaml
    /// </summary>
    public partial class Window_ProcurementScheduler : Window
    {
        public Window_ProcurementScheduler()
        {
            InitializeComponent();
        }

        private async void Button_Click(object sender, RoutedEventArgs e)
        {
            Random rand = new Random();
            List<Urok17.Products> listProducts = new List<Urok17.Products>();

            foreach (ProductType type in DBManager.getInstance().GetProductTypes())
            {
                listProducts.Add(new Products(2.2, 15, type, new TimeSpan(rand.Next(1, 14), 0, 0, 0)));
                listProducts.Add(new Products(3.3, 3, type, new TimeSpan(rand.Next(1, 14), 0, 0, 0)));
            }



            //var sched = Scheduler.CreateScheduleProcurement(listProducts, DateTime.Now, DateTime.Now + new TimeSpan(180, 0, 0, 0));
            //var sched = DBManager.getInstance().CallStoredProcedure(DateTime.Now, DateTime.Now + new TimeSpan(180, 0, 0, 0));
             var sched = await DBManager.getInstance().CallStoredProcedureAsync(DateTime.Now, DateTime.Now + new TimeSpan(180, 0, 0, 0));
            //await DBManager.getInstance().Call_GetParamOUTAsync();






             /// Переформатируем
             Dictionary<DateTime, string> dick = new Dictionary<DateTime, string>();
             foreach (var k in sched)
             {
                 var dispt = k.Value.Distinct();         // составили список уникальных продуктов для текущего дня
                 StringBuilder s = new StringBuilder();

                 foreach (ProductType pt in dispt)       // Упаковали в одну строку
                 {
                     int count = k.Value.Where(elem => elem == pt).Count();
                     if (count > 1)
                         s.AppendFormat("{0}x {1} / ", count, pt);
                     else
                         s.AppendFormat("{0} / ", pt);
                 }
                 dick.Add(k.Key, s.ToString());
             }

             //dtgrdProcurement.ItemsSource = sched;
             //dtgrdProcurement.Items.Refresh();

             dtgrdProcurement.ItemsSource = dick;
             dtgrdProcurement.Items.Refresh();
        }
    }
}
