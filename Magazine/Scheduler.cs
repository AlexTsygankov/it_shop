﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Urok17;

namespace Magazine
{
    class Scheduler
    {

        /// <summary>
        /// Возвращает график закупок, упорядоченный по дате, на промежуток
        /// времени от DateFrom до DateTo. Если в исходном списке продуктов были продукты 
        /// одного и того же типа 
        /// </summary>
        /// <param name="needProducts"> Список необходимых продуктов</param>
        /// <param name="DateFrom">С какого числа?</param>
        /// <param name="DateTo">По какое число?</param>
        /// <returns>Словарь вида  дата-что купить</returns>
        public static SortedDictionary<DateTime, List<ProductType>> CreateScheduleProcurement(List<Products> needProducts, DateTime DateFrom, DateTime DateTo)
        {
            if(DateTo.CompareTo(DateFrom) <= 0)
                throw new ArgumentOutOfRangeException("DateTo", "Неверно указан интервал времени. DateTo дожен быть позже чем DateFrom");
            
            var schedule = new SortedDictionary<DateTime, List<ProductType>>();

            if ((DateTo - DateFrom).TotalDays < 1)
                throw new ArgumentException("DateTo", "Промежуток времени должен составлять 1 и более дней");

            // берём список продуктов...
            foreach(Products prod in needProducts)
            {
                if (prod.ExpirationDate < TimeSpan.FromDays(0.5))
                    throw new ArgumentException("ExpirationDate", "Срок годлности продукта не должен быть меньше оловины суток");

                // и указываем когда нужно будет купить продукт, исходя из его срока годности
                for (var tickDate = DateFrom;  
                        DateTo - tickDate > TimeSpan.FromDays(0.5);
                        tickDate += prod.ExpirationDate)
                {
                    var insetrtableDate = tickDate + prod.ExpirationDate;

                    if (insetrtableDate < DateTo)
                    {
                        if (schedule.ContainsKey(insetrtableDate))
                            schedule[insetrtableDate].Add(prod.Type);
                        else
                            schedule.Add(insetrtableDate, new List<ProductType>() { prod.Type });
                        }
                }
            }
            return schedule;
        }
    }
}
