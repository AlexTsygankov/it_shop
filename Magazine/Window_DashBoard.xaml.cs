﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

using System.Data.SqlClient;
using System.Data;

namespace Magazine
{
    /// <summary>
    /// Логика взаимодействия для Window_DashBoard.xaml
    /// </summary>
    public partial class Window_DashBoard : Window, System.ComponentModel.INotifyPropertyChanged
    {

        private List<Window> OpenedWindows = new List<Window>();
        private Shop shop { get { return (Shop)Resources["ShopResource"]; } }

        public ConnectionState ConStatus { get { return DBManager.getInstance().Status; } }

        public Window_DashBoard()
        {
            InitializeComponent();
            shop.Name = "Всякая Всячина";
            shop.Money = 153000;

            DBManager.getInstance().PropertyChanged += Window_DashBoard_PropertyChanged;
        }

        void Window_DashBoard_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (e.PropertyName == "Status")
                NotifyPropertyChanged("ConStatus");
        }

        

        private void btnOpenStorage_Click(object sender, RoutedEventArgs e)
        {
            if (OpenedWindows.Where(w => w is Window_Storage).Count() < 1)
            {
                //if (dbConnection != null && dbConnection.State == ConnectionState.Open)

                //LoadProductTypesFromDB();

                Window_Storage win = new Window_Storage();
                win.SetModel(this.shop);
                win.Closed +=win_Closed;
                OpenedWindows.Add(win);
                win.Show();
            }
            
        }

        private void btnOpenEditor_Click(object sender, RoutedEventArgs e)
        {
            if (OpenedWindows.Where(w => w is Window_TypesEditor).Count() < 1)
            {
                Window_TypesEditor win = new Window_TypesEditor();
                win.SetModel(this.shop);

                win.Closed += win_Closed;


                //string SaveFile = Environment.CurrentDirectory + @"\types.txt";

                //if (System.IO.File.Exists(SaveFile))
                //    win.ViewModel.LoadTypesFrom(SaveFile);
                win.Show();
                OpenedWindows.Add(win);
            }
            
        }

        void win_Closed(object sender, EventArgs e)
        {
            if (sender is Window)
                OpenedWindows.Remove((Window)sender);
        }

        private void Window_Closed(object sender, EventArgs e)
        {
            OpenedWindows.ForEach(w => w.Close());
            DBManager.getInstance().CloseConnection();
       }

        private void btnConnect_Click(object sender, RoutedEventArgs e)
        {
            if (this.ConStatus == ConnectionState.Broken ||
                ConStatus == ConnectionState.Closed)
            {
                DBManager.getInstance().ReconnectToDB();
            }
        }

        //void dbConnection_StateChange(object sender, StateChangeEventArgs e)
        //{
        //    lblConnectionState.Content = dbConnection.State;
        //}
        
        //private void ConnectToDB()
        //{
        //    const string connectionString = @"Data Source=ALEXPC\SQLEXPRESS;"+
        //                                     "Initial Catalog=it_magaz;"+
        //                                     "Integrated Security=true";
        //    if (dbConnection == null)
        //    {
        //        dbConnection = new SqlConnection(connectionString);
        //        dbConnection.StateChange += dbConnection_StateChange;

        //        try{
        //            dbConnection.Open();
        //        }
        //        catch (SqlException ex)
        //        {
        //            MessageBox.Show(ex.Message);
        //            dbConnection.Close();
        //            dbConnection.Dispose();
        //        }
        //    }
        //}
        
        

        private void btnDisconnect_Click(object sender, RoutedEventArgs e)
        {
            /// Проверим статус текущего соединения 
            if (ConStatus == ConnectionState.Executing ||
                ConStatus == ConnectionState.Fetching)
            {
                /// Предупредим об активном соединнении
                if (MessageBox.Show("В данный момент, приложение общается с базой данных." +
                    " Вы действительно хотите разорвать соединение?", "Предупреждение",
                    MessageBoxButton.YesNoCancel,
                    MessageBoxImage.Question) == MessageBoxResult.Yes)
                {
                    DBManager.getInstance().CloseConnection();
                }
            }
            else {
                DBManager.getInstance().CloseConnection();
            }
        }
        #region INotifyPropertyChanged implementation
        /// <summary>
        /// Событие происходит при изменение каждого свойства
        /// </summary>
        public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;

        // атрибут CallerMemberName позволяет не писать каждый раз имя проперти вручную
        private void NotifyPropertyChanged([System.Runtime.CompilerServices.CallerMemberName] String propertyName = "")
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new System.ComponentModel.PropertyChangedEventArgs(propertyName));
            }
        }
        #endregion

        private void btnOpenScheduler_Click(object sender, RoutedEventArgs e)
        {
            if (OpenedWindows.Where(w => w is Window_ProcurementScheduler).Count() < 1)
            {
                var win = new Window_ProcurementScheduler();

                win.Closed += win_Closed;


                //string SaveFile = Environment.CurrentDirectory + @"\types.txt";

                //if (System.IO.File.Exists(SaveFile))
                //    win.ViewModel.LoadTypesFrom(SaveFile);
                win.Show();
                OpenedWindows.Add(win);
            }
        }
    }
}
