﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Data.SqlClient;
using System.Data;

using Urok17;

namespace Magazine
{
    public class DBManager:System.ComponentModel.INotifyPropertyChanged
    {
        private static SqlConnection dbConnection;

        private static DBManager instance = new DBManager();

        public static DBManager getInstance()
        {
            return instance;
        }
        

        public ConnectionState Status { get { return _status; } }
        private ConnectionState _status;

        private DBManager()
        {
            ConnectToDB();            
        }
        private void ConnectToDB()
        {
            const string connectionString = @"Data Source=ALEXPC\SQLEXPRESS;" +
                                             "Initial Catalog=it_magaz;" +
                                             "Integrated Security=true";
            if (dbConnection == null)
            {
                dbConnection = new SqlConnection(connectionString);
                dbConnection.StateChange += dbConnection_StateChange;
            }
                try
                {
                    dbConnection.Open();
                }
                catch (SqlException ex)
                {
                    dbConnection.Close();
                    dbConnection.Dispose();
                }
        }

        void dbConnection_StateChange(object sender, StateChangeEventArgs e)
        {
            _status = e.CurrentState;
            NotifyPropertyChanged("Status");
        }

        /// <summary>
        /// Закрывает соединение с сервером
        /// </summary>
        public void CloseConnection()
        {
            if (dbConnection != null)
                dbConnection.Close();
        }
        internal void ReconnectToDB()
        {
            CloseConnection();
            ConnectToDB();
        }

        

        public IEnumerable<ProductType> GetProductTypes()
        {
            SqlConnection connection = dbConnection;

            List<ProductType> DBProductTypes = new List<ProductType>();

            SqlCommand command = new SqlCommand(
                "SELECT Subtype_name, Type_name FROM Types LEFT JOIN " +
                "Subtypes ON Subtypes.Type = Types.id;"
                , connection);
            try
            {
                using (SqlDataReader reader = command.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        string name, subname;
                        name = reader["Type_name"].ToString();
                        subname = reader["subtype_name"].ToString();

                        if (subname == "NULL") subname = "";

                        DBProductTypes.Add(new ProductType(name, subname));
                    }
                }
            }
            catch (SqlException ex)
            {
                throw;
            }
            return DBProductTypes;
        }

        

        private void GetIndexAvailableToInsertFromTable(string table, out int availableIndex)
        {
            // Получение свободногo индекса
            SqlCommand commFirstIndx = new SqlCommand(
                "SELECT TOP 1 id FROM " + table + " " +
                "ORDER BY id"
                , dbConnection);

            try
            {
                using (SqlDataReader reader = commFirstIndx.ExecuteReader())
                {
                    reader.Read();
                    availableIndex = reader.GetInt32(0);
                    availableIndex--;   // походу костыль, так нужно базе...
                }
            }
            catch (SqlException ex)
            {
                throw;
            }

            if (availableIndex <= 1)
            {
                SqlCommand commLastIndx = new SqlCommand(
                    "SELECT TOP 1 id FROM " + table + " " +
                    "ORDER BY id DESC"
                    , dbConnection);
                try
                {
                    using (SqlDataReader reader = commLastIndx.ExecuteReader())
                    {
                        reader.Read();
                        availableIndex = reader.GetInt32(0);
                        availableIndex++;
                    }
                }
                catch (SqlException ex)
                {
                    throw;
                }
            }
        }
        /// <summary>
        /// Возвращает индекс внешнего ключа
        /// </summary>
        /// <param name="table"></param>
        /// <param name="type"></param>
        /// <param name="index"></param>
        private void GetForeignKeyIndexByField(string table, string type, out int index)
        {
            SqlCommand comTypeID = new SqlCommand(
                "SELECT id FROM " + table + " WHERE Type_name = @Tp_nm",
                dbConnection);

            //comTypeID.Parameters.AddWithValue("@Table_name", table);
            comTypeID.Parameters.AddWithValue("@Tp_nm", type);

            using (SqlDataReader reader = comTypeID.ExecuteReader())
            {
                reader.Read();
                index = reader.GetInt32(0);
            }
        }
        
        public void AddSubtype(string subtype, string type)
        {
            if (dbConnection.State == ConnectionState.Open)
            {
                //await Task.Run(() =>
                //    {
                        int index;
                        int fk_type;

                        try
                        {
                            GetIndexAvailableToInsertFromTable("Subtypes", out index);
                            GetForeignKeyIndexByField("Types", type, out fk_type);
                        }
                        catch (Exception ex) { throw; }


                        /// Вставка
                        /// Запрос на вставку с параметрами
                        string query = "INSERT INTO Subtypes (id, Subtype_name, Type) VALUES(@id, @Subtype_name, @Type)";

                        SqlCommand commInsertType = new SqlCommand(query, dbConnection);
                        commInsertType.Parameters.AddWithValue("@id", index);
                        commInsertType.Parameters.AddWithValue("@Subtype_name", subtype);
                        commInsertType.Parameters.AddWithValue("@Type", fk_type);

                        try
                        {
                            commInsertType.ExecuteNonQuery();
                        }
                        catch (SqlException ex) { throw; }
                    //});
            }
        }

        public void AddType(string type)
        {
            if (dbConnection.State == ConnectionState.Open)
            {
                int index;

                try
                {
                    GetIndexAvailableToInsertFromTable("Types", out index);
                }
                catch (Exception ex) { throw; }

                /// Вставка
                /// Запрос на вставку с параметрами
                string query = "INSERT INTO Types (id, Type_name) VALUES(@id,@Type_name)";

                SqlCommand commInsertType = new SqlCommand(query, dbConnection);
                commInsertType.Parameters.AddWithValue("@id", index);
                commInsertType.Parameters.AddWithValue("@Type_name", type);

                try
                {
                    commInsertType.ExecuteNonQuery();
                }
                catch (SqlException ex)
                {
                    throw;
                }
            }
        }

        public void RemoveType(string type)
        {
            ///// Удаляем зависимости 
            ///// Здесь удаляются зависимости из таблицы Subtypes

            #region
            //int fk_type;

            //try
            //{
            //    GetForeignKeyIndexByField("Types", type, out fk_type);
            //}
            //catch (Exception ex) { throw; }

            //SqlCommand comDeleteConstrainsSubtypes = new SqlCommand(
            //    "DELETE FROM Subtypes WHERE Type = @FK_type",
            //    dbConnection);

            //comDeleteConstrainsSubtypes.Parameters.AddWithValue("@FK_type", fk_type);

            //try { comDeleteConstrainsSubtypes.ExecuteNonQuery(); }
            //catch (Exception ex) { throw; }

            ///
            ///-----Удалям само значание------//
            ///
            #endregion

            string query = "DELETE FROM Types WHERE Type_name = @Type_name";

            SqlCommand commInsertType = new SqlCommand(query, dbConnection);
            commInsertType.Parameters.AddWithValue("@Type_name", type);

            try
            {
                commInsertType.ExecuteNonQuery();
            }
            catch (SqlException ex)
            {
                throw;
            }
        }

        internal void RemoveSubtype(string subtype, string type)
        {
            int fk_type;

            try
            {
                GetForeignKeyIndexByField("Types", type, out fk_type);
            }
            catch (Exception ex) { throw; }

            SqlCommand comDeleteConstrainsSubtypes = new SqlCommand(
                "DELETE FROM Subtypes " +
                "WHERE Subtype_name = @SBtp_name AND Type = @FK_type",
                dbConnection);

            comDeleteConstrainsSubtypes.Parameters.AddWithValue("@FK_type", fk_type);
            comDeleteConstrainsSubtypes.Parameters.AddWithValue("@SBtp_name", subtype);

            try { comDeleteConstrainsSubtypes.ExecuteNonQuery(); }
            catch (Exception ex) { throw; }
        }

        public int UpdateTypes(string oldname, string newname)
        {
            /// "UPDATE table_name SET col_name = value ON col_name = old_name;" //

            SqlCommand comUpdate = new SqlCommand(
                "UPDATE Types SET Type_name = @New_name WHERE Type_name=@OldName"
                , dbConnection);
            comUpdate.Parameters.AddWithValue("@New_name", newname);
            comUpdate.Parameters.AddWithValue("@OldName", oldname);

            try
            {
                return comUpdate.ExecuteNonQuery();
            }
            catch (Exception) { throw; }
        }
        public int UpdateSubtypes(string oldname, string newname)
        {
            SqlCommand comUpdate = new SqlCommand(
               "UPDATE Subtypes SET Subtype_name = @New_name "+
               "WHERE Subtype_name = @OldName"
               , dbConnection);
            comUpdate.Parameters.AddWithValue("@New_name", newname);
            comUpdate.Parameters.AddWithValue("@OldName", oldname);

            try
            {
                return comUpdate.ExecuteNonQuery();
            }
            catch (Exception) { throw; }
        }

        public SortedDictionary<DateTime, List<ProductType>> GetScheduleProcurement(/*List<Products> needProducts,*/ DateTime DateFrom, DateTime DateTo)
        {

            SqlCommand comGet = new SqlCommand(
                @"DECLARE @t table(date_span datetime) "+
                @"DECLARE @dat datetime "+
                @"SET @dat = @start_date "+
                @"WHILE @dat < @end_date "+
                @"BEGIN "+
                @"SET @dat = DATEADD(day, 1, @dat) "+
                @"INSERT INTO @t "+
                @"VALUES (@dat) "+
                @"END "+
                @"SET @dat = @start_date	"+
                @"SELECT date_span, Subtype_name, Types.Type_name 	"+
                @"FROM @t, Subtypes  inner join Types on Subtypes.Type = Types.id " +
                @"WHERE DATEDIFF(DAY, @dat, date_span) % ExpirationSpan = 0; "
                , dbConnection);
            comGet.Parameters.AddWithValue("@start_date", DateFrom);
            comGet.Parameters.AddWithValue("@end_date", DateTo);

            var schedule = new SortedDictionary<DateTime, List<ProductType>>();

            try{
                using (SqlDataReader  reader = comGet.ExecuteReader())
                {
                    while (reader.Read()) 
                    {
                        var insetrtableDate = DateTime.Parse(reader["date_span"].ToString());

                        var prod = new ProductType(reader["Subtype_name"].ToString(), reader["Type_name"].ToString());

                        if (schedule.ContainsKey(insetrtableDate))
                            schedule[insetrtableDate].Add(prod);
                        else
                            schedule.Add(insetrtableDate, new List<ProductType>() { prod});
                    }
                }
            }
            catch(SqlException exep){throw;}

            return schedule;
        }



        public SortedDictionary<DateTime, List<ProductType>> CallStoredProcedure(DateTime DateFrom, DateTime DateTo)
        {
            SqlCommand command = new SqlCommand(
                "Get_Schedule_params"
                ,dbConnection);

            command.CommandType = CommandType.StoredProcedure;

            SqlParameter start_date = new SqlParameter();
            start_date.ParameterName = "@start_date";
            start_date.Direction = ParameterDirection.Input;
            start_date.SqlDbType = SqlDbType.DateTime;

            start_date.Value = DateFrom;

            SqlParameter end_date = new SqlParameter();
            end_date.ParameterName = "@end_date";
            end_date.Direction = ParameterDirection.Input;
            end_date.SqlDbType = SqlDbType.DateTime;

            end_date.Value = DateTo;

            command.Parameters.Add(start_date);
            command.Parameters.Add(end_date);


            var schedule = new SortedDictionary<DateTime, List<ProductType>>();

            try
            {
                using (SqlDataReader reader = command.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        var insetrtableDate = DateTime.Parse(reader["date_span"].ToString());

                        var prod = new ProductType(reader["Subtype_name"].ToString(), reader["Type_name"].ToString());

                        if (schedule.ContainsKey(insetrtableDate))
                            schedule[insetrtableDate].Add(prod);
                        else
                            schedule.Add(insetrtableDate, new List<ProductType>() { prod });
                    }
                }
            }
            catch (SqlException exep) { throw; }

            return schedule;
            
        }

        public async Task<SortedDictionary<DateTime, List<ProductType>>> CallStoredProcedureAsync(DateTime DateFrom, DateTime DateTo)
        {
            return await Task<SortedDictionary<DateTime, List<ProductType>>>.Run( () =>
                CallStoredProcedure(DateFrom, DateTo));
        }

        public void Call_GetParamOUT()
        {
            SqlCommand command = new SqlCommand(
                "GetParamOUT_2"
                , dbConnection);

            command.CommandType = CommandType.StoredProcedure;

            SqlParameter outparam = new SqlParameter();
            outparam.ParameterName = "@outParam";
            outparam.Direction = ParameterDirection.ReturnValue;
            outparam.SqlDbType = SqlDbType.NVarChar;

            command.ExecuteNonQuery();

            string s = command.Parameters["@outParam"].Value.ToString();
        }

        public async Task Call_GetParamOUTAsync()
        {
            await Task.Run(() =>
                Call_GetParamOUT()
                );
        }


        public DateTime GetExpiration(string subtype)
        {
            throw new NotImplementedException();
        }



        #region INotifyPropertyChanged implementation
        /// <summary>
        /// Событие происходит при изменение каждого свойства
        /// </summary>
        public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;

        // атрибут CallerMemberName позволяет не писать каждый раз имя проперти вручную
        private void NotifyPropertyChanged([System.Runtime.CompilerServices.CallerMemberName] String propertyName = "")
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new System.ComponentModel.PropertyChangedEventArgs(propertyName));
            }
        }
        #endregion
    }
}
