﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Magazine
{
    /// <summary>
    /// Логика взаимодействия для Window_UpdateType.xaml
    /// </summary>
    public partial class Window_UpdateType : Window, System.ComponentModel.INotifyPropertyChanged
    {
        string _oldName;
        //private string TypeName
        //{
        //    get { return _name; }
        //    set
        //    {
        //        if (value != _name)
        //        {
        //            _name = value;
        //            NotifyPropertyChanged();
        //        }
        //    }
        //}

        public TextBlock Sender
        {
            get { return _sender; }
            set
            {
                if (value != _sender)
                {
                    _sender = value;
                    _oldName = _sender.Text;
                    NotifyPropertyChanged();
                }
            }
        }
        private TextBlock _sender;

        public Window_UpdateType()
        {
            InitializeComponent();
            //TypeName = "akfjbksdj";
        }

        /// <summary>
        /// Событие происходит при изменение каждого свойства
        /// </summary>
        public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;
        // атрибут CallerMemberName позволяет не писать каждый раз имя проперти вручную
        private void NotifyPropertyChanged([System.Runtime.CompilerServices.CallerMemberName] String propertyName = "")
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new System.ComponentModel.PropertyChangedEventArgs(propertyName));
            }
        }

        private void btnUpdate_Click(object sender, RoutedEventArgs e)
        {
            if (this.Owner is Window_TypesEditor)
            {
                ((Window_TypesEditor)Owner).ChangeTextBlockText(Sender, _oldName, Sender.Text);
            }
            Close();
        }
    }
}
