﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Magazine
{
    /// <summary>
    /// Содержит модель магазина
    /// </summary>
    public class ShopContainer : System.ComponentModel.INotifyPropertyChanged
    {
        /// <summary>
        /// Модель вашего магазина
        /// </summary>
        public Shop Model
        {
            get { return _model; }
            set
            {
                if (value != _model)
                {
                    _model = value;
                    _model.PropertyChanged += _model_PropertyChanged;
                    NotifyPropertyChanged();
                }
            }
        }
        private Shop _model;


        void _model_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            this.NotifyPropertyChanged(e.PropertyName);
        }
        


        /// <summary>
        /// Событие происходит при изменение каждого свойства
        /// </summary>
        public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;

        /// атрибут CallerMemberName позволяет не писать каждый раз имя проперти вручную
        private void NotifyPropertyChanged([System.Runtime.CompilerServices.CallerMemberName] String propertyName = "")
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new System.ComponentModel.PropertyChangedEventArgs(propertyName));
            }
        }

        
    }
}
