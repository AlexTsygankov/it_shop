﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Magazine;
using System.Runtime.CompilerServices;
using System.ComponentModel;

namespace Urok17
{
    public class Products: System.ComponentModel.INotifyPropertyChanged
    {
        private double _price;
        /// <summary>
        /// Цена продукта
        /// </summary>
        public double priceProperti
        {
            get
            {
                return _price;
            }
            set
            {
                if (value > 0)
                {
                    _price = value;
                    NotifyPropertyChanged();
                }
            }
        }

        private double _weight;
        /// <summary>
        /// Вес продукта
        /// </summary>
        public double weightProperti
        {
            get
            {
                return _weight;
            }
            set
            {
                if (value > 0)
                {
                    _weight = value;
                    NotifyPropertyChanged();
                }
            }
        }
        /// <summary>
        /// Название / тип продукта
        /// </summary>
        public string nameProperti
        {
            get
            {
                return Type.Name;
            }
        }

        /// <summary>
        /// Под тип продукции
        /// </summary>
        public string Subname
        {
            get { return Type.SubType; }
            //set { _subname = value; }
        }

        private string _producer;
        /// <summary>
        /// Производитель
        /// </summary>
        public string Producer { get { return _producer; }
            set
            {
                if (value != _producer)
                {
                    _producer = value;
                    NotifyPropertyChanged();
                }
            }
        }

        private TimeSpan _expiration;
        /// <summary>
        /// Длительность в днях
        /// </summary>
        public TimeSpan ExpirationDate
        {
            get { return _expiration; }
            set
            {
                if (value != _expiration)
                {
                    _expiration = value;
                    NotifyPropertyChanged();
                }
            }
        }

        private DateTime _creationDate;
        /// <summary>
        /// Точность до дня
        /// </summary>
        public DateTime CreationDate
        {
            get { return _creationDate; }
            set
            {
                if (value != _creationDate)
                {
                    _creationDate = value;
                    NotifyPropertyChanged();
                }
            }
        }


        //public virtual void show()
        //{
        //    //Console.WriteLine("Название:{0}\nЦена:{1}\nВес:{2}\n", name, price, weight); 
        //    throw new NotImplementedException();
        //}



        /// <summary>
        /// Создаёт пустой продукт, не имеющий веса, цены, производителя и типа.
        /// </summary>
        public Products()
        {
            this._weight = 0.0;            
            this._producer = "";
            this._price = 0.0;            
            this._type = new ProductType();

            this._expiration = TimeSpan.MinValue;
            this._creationDate = DateTime.Now;
        }

        public Products(double weight, int price, ProductType type, TimeSpan expiration)
        {
            this._weight = weight;
            this._producer = "";
            this._price = price;
            this._type = type;

            this._expiration = expiration;
            this._creationDate = DateTime.Now;
        }

        public virtual void vvodprod(ProductType type, double price, double weight)
        {
            _type = type;
            priceProperti = price;
            weightProperti = weight;
        }

        /// <summary>
        /// Сериализует объект. Разделитель |
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return string.Format("Наименование:{0}|Подтип:{1}|Вес:{2}|СрокГодн:{3}", nameProperti, Subname, _weight, _expiration);
        }

        /// <summary>
        /// Тип продукта. Только чтение.
        /// </summary>
        public ProductType Type
        {
            get
            {
                return _type;
            }
        }
        private ProductType _type;


        /// <summary>
        /// Событие происходит при изменение каждого свойства
        /// </summary>
        public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;
        
        // атрибут CallerMemberName позволяет не писать каждый раз имя проперти вручную
        private void NotifyPropertyChanged([CallerMemberName] String propertyName = "")
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    }
}
